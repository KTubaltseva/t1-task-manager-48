package ru.t1.ktubaltseva.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface IHasDescription {

    @Nullable
    String getDescription();

}
