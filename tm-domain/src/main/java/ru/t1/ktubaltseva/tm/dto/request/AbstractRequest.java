package ru.t1.ktubaltseva.tm.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public abstract class AbstractRequest implements Serializable {

}
