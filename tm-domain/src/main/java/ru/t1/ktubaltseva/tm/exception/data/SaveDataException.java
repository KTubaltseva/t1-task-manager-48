package ru.t1.ktubaltseva.tm.exception.data;

public class SaveDataException extends AbstractDataException {

    public SaveDataException() {
        super("Error! Failed to save data...");
    }

}
