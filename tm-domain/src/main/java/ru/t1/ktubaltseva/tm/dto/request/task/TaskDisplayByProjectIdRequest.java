package ru.t1.ktubaltseva.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskDisplayByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskDisplayByProjectIdRequest(@Nullable final String token) {
        super(token);
    }

    public TaskDisplayByProjectIdRequest(@Nullable final String token, @Nullable final String projectId) {
        super(token);
        this.projectId = projectId;
    }

}
