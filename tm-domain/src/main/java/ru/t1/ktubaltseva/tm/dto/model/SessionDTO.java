package ru.t1.ktubaltseva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.model.IHasCreated;
import ru.t1.ktubaltseva.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SessionDTO extends AbstractUserOwnedModelDTO implements IHasCreated {

    private static final long serialVersionUID = 1;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role", columnDefinition = "varchar(30)", nullable = false)
    private Role role = Role.USUAL;

    public SessionDTO(@NotNull final UserDTO user) {
        this.setUserId(user.getId());
    }

}
