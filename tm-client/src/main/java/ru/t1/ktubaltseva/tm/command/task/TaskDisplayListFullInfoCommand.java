package ru.t1.ktubaltseva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.request.task.TaskDisplayListRequest;
import ru.t1.ktubaltseva.tm.dto.response.task.TaskDisplayListResponse;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskDisplayListFullInfoCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-list-full-info";

    @NotNull
    private final String DESC = "Display task list (full info).";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DISPLAY TASKS FULL]");
        System.out.println("[ENTER SORT]:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskDisplayListRequest request = new TaskDisplayListRequest(getToken(), sort);
        @NotNull final TaskDisplayListResponse response = getTaskEndpoint().getAllTasks(request);
        @Nullable final List<TaskDTO> tasks = response.getTasks();
        renderTasksFullInfo(tasks);
    }

}
