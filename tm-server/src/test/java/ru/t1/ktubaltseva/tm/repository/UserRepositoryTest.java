package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.dto.IUserDTORepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.repository.dto.UserDTORepository;
import ru.t1.ktubaltseva.tm.service.ConnectionService;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.Collection;

import static ru.t1.ktubaltseva.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest extends AbstractRequestTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    private final IUserDTORepository repository = new UserDTORepository(entityManager);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        if (entityManager.getTransaction().isActive())
            entityManager.getTransaction().rollback();
    }

    @After
    @SneakyThrows
    public void after() {
        for (@NotNull final UserDTO user : USER_LIST) {
            try {
                entityManager.getTransaction().begin();
                repository.removeById(user.getId());
                entityManager.getTransaction().commit();
            } catch (@NotNull final Exception e) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final UserDTO userToAdd = USER_1;
        @Nullable final String userToAddId = userToAdd.getId();

        entityManager.getTransaction().begin();
        repository.add((userToAdd));
        entityManager.getTransaction().commit();

        @Nullable final UserDTO userFindOneById = repository.findOneById(userToAddId);
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(userToAdd.getId(), userFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final UserDTO userExists = USER_1;

        entityManager.getTransaction().begin();
        repository.add(userExists);
        entityManager.getTransaction().commit();

        @Nullable final UserDTO userFindOneById = repository.findOneById(userExists.getId());
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(userExists.getId(), userFindOneById.getId());

        @Nullable final UserDTO userFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_USER_ID);
        Assert.assertNull(userFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findByEmail() {
        @NotNull final UserDTO userWithEmail = USER_WITH_LOGIN_EMAIL;

        entityManager.getTransaction().begin();
        repository.add(userWithEmail);
        entityManager.getTransaction().commit();

        @Nullable final UserDTO userFindByNotExistentEmail = repository.findByEmail(NON_EXISTENT_USER_EMAIL);
        Assert.assertNull(userFindByNotExistentEmail);

        @Nullable final UserDTO userFindByEmail = repository.findByEmail(USER_EMAIL);
        Assert.assertNotNull(userFindByEmail);
        Assert.assertEquals(userWithEmail.getId(), userFindByEmail.getId());
    }

    @Test
    @SneakyThrows
    public void findByLogin() {
        @NotNull final UserDTO userWithLogin = USER_WITH_LOGIN_EMAIL;

        entityManager.getTransaction().begin();
        repository.add(userWithLogin);
        entityManager.getTransaction().commit();

        @Nullable final UserDTO userFindByNotExistentLogin = repository.findByLogin(NON_EXISTENT_USER_LOGIN);
        Assert.assertNull(userFindByNotExistentLogin);

        @Nullable final UserDTO userFindByLogin = repository.findByLogin(USER_LOGIN);
        Assert.assertNotNull(userFindByLogin);
        Assert.assertEquals(userWithLogin.getId(), userFindByLogin.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final UserDTO userExists = USER_1;

        entityManager.getTransaction().begin();
        repository.add(userExists);
        entityManager.getTransaction().commit();

        @NotNull final Collection<UserDTO> usersFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(usersFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final UserDTO userToRemove = USER_1;

        entityManager.getTransaction().begin();
        repository.add((userToRemove));
        entityManager.getTransaction().commit();

        entityManager.getTransaction().begin();
        repository.removeById(userToRemove.getId());
        entityManager.getTransaction().commit();

        @Nullable final UserDTO userFindOneById = repository.findOneById(userToRemove.getId());
        Assert.assertNull(userFindOneById);
    }

}
