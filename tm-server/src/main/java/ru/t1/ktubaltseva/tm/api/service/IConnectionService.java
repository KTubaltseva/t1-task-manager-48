package ru.t1.ktubaltseva.tm.api.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IConnectionService {

    @NotNull
    @SneakyThrows
    EntityManager getEntityManager();

    @NotNull
    @SneakyThrows
    Liquibase getLiquibase();

}
