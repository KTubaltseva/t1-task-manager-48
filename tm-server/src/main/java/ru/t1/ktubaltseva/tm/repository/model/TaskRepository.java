package ru.t1.ktubaltseva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.repository.model.ITaskRepository;
import ru.t1.ktubaltseva.tm.comparator.CreatedComparator;
import ru.t1.ktubaltseva.tm.comparator.NameComparator;
import ru.t1.ktubaltseva.tm.comparator.StatusComparator;
import ru.t1.ktubaltseva.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedWBSRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<Task> getClazz() {
        return Task.class;
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " m " +
                "WHERE m.user.id = :userId " +
                "and m.project.id = :projectId";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName() + " m " +
                "WHERE m.user.id = :userId " +
                "and m.project.id = :projectId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
